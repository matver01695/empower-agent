EmPOWER packages for OpenWrt
===========

Setup in OpenWrt:

```
  $: cd $TOPDIR
  $: echo 'src-git empower https://bitbucket.org/matver01695/empower-agent.git' >> feeds.conf.default
  $: ./scripts/feeds update empower
  $: ./scripts/feeds install -a -p empower
  $: make menuconfig
  $: select Network -> empower-agent
```

